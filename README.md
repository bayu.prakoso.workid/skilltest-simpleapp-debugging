# Skill Test - Simple App Debugging

In this part of the test, I did some changing and adjust the css styling to solve errors and fix the website styling. I'll explain it one by one.

### Solve Compile-time Errors

After do `yarn install` and then do `yarn serve`, I encounter some compile-time errors in Users.vue. Below are the errors:

- 35:10 error 'mapState' is defined but never used no-unused-vars
- 36:10 error 'INCREMENT' is defined but never used no-unused-vars
- 57:15 error 'error' is defined but never used no-unused-vars

Those errors produce by es-lint because of unused variable from import. I changed the code to solve those errors by doing this:

- for 'mapState', I replaced it by mapGetters because I realized the state is already serve with getters and those getters is already implemented in Users.vue.

      	Before:

      			import { mapState } from "vuex";

      	After:

      			import { mapGetters } from "vuex";

So I use mapGetters and also change how the computed codes looks like:

    Before:
    computed: {
    users() {
      return this.$store.getters.getData;
    },
    count() {
      return this.$store.getters.getCountData;
    },
    },

    After:
    computed: {
    ...mapGetters({
      users: "getData",
      count: "getCountData",
    }),
    },

Now code results is simpler to read.

- for `INCREMENT` , I checked it on mutation-types.js and try to search the implementation of those variable as a mutation or anything else and I found nothing. So I decided to remove INCREMENT import from the code.
- for `error`, it happen because error parameter is provide by catch function but it never user. Finally I try to use it on `console.log`. Also I deleted `this.loading` because those local state never exist and not implemented on any components.

      		.catch((error) => {
        JSON.stringify(error);});

### Fixing naming bug

I fixed naming action on Users.vue. This bug is produced because of action naming defined on store.js was not the same with Users.vue implementation:
Before:
this.$store.dispatch("setData", data);
	
	After:
	this.$store.dispatch("setDatas", data);

I fixed naming mutation store.js. This bug is produced because of mutation naming defined on mutation-types.js was not the same with store.js implementation:

    Before:
    context.commit('setData', data);

    After:
    context.commit(SET_DATA, data);

I fixed naming state store.js. This bug is produced because of mutation naming defined on state was not the same with mutation implementation:

    Before:
    state: {
    list: null
    },

    After:
    state: {
    lists: null
    },

### Change data type and return value on store.js

I Change data type on state lists to avoid some undefined value from null value
Before:
state: {
list: null
},
After:
state: {
lists: []
},

Change getCountData getters return value from 0 to length of state lists

    Before:
    getCountData: () => {
        return 0
    }

    After:
    getCountData: (state) => {
      return state.lists.length;
    },
    },

### Change key v-for

I change key from name to id, because name can be consist some duplicated value

before:

    	<div v-for="user in users" :key="user.name" class="card">

after:

    	<div v-for="user in users" :key="user.id" class="card">

### Styling Fixing

After bugs is already fixed, I adjust the styling (do some change and add on css) so the web app result is as close as possible with the mockup expectation. I use the class name that already define on template. For lay-outing I mainly use grid and flexbox. Here it is the style code on Users.vue:

    	.main {
    	  background-color: #f5f7fa;
    	  padding: 20px;
    	}
    	.main-title {
    	  font-size: 30px;
    	  font-weight: bold;
    	}
    	.count {
    	  color: #83dd8b;
    	  font-weight: 800;
    	  margin-right: 10px;
    	}
    	.user-count {
    	  border-bottom: 1px solid #e1e1e7;
    	  color: #646464;
    	  display: flex;
    	}
    	.wrapper {
    	  display: grid;
    	  grid-gap: 1rem;
    	  grid-template-columns: repeat(2, 1fr);
    	}
    	.card {
    	  background-color: #ffffff;
    	  padding: 30px;
    	  margin-top: 20px;
    	}
    	.body-card {
    	  display: flex;
    	  flex-direction: column;
    	}
    	.title {
    	  color: #cccaca;
    	  font-weight: bold;
    	}
    	.name {
    	  font-weight: bold;
    	  font-size: 25px;
    	  color: #4e4e4e;
    	}
    	.wrapper-card {
    	  display: flex;
    	  flex-direction: row;
    	  justify-content: space-between;
    	  margin-top: 20px;
    	}
    	.email {
    	  font-weight: bold;
    	  font-size: 18px;
    	  color: #4e4e4e;
    	}
    	.phone {
    	  font-weight: bold;
    	  font-size: 18px;
    	  color: #4e4e4e;
    	}
    	.website {
    	  font-weight: bold;
    	  font-size: 18px;
    	  color: #83dd8b;
    	}

# vue-test-code

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

Expected Output :
![expected](./src/assets/Expected.png)
